
variable "vpc_cidr" {
  type    = string
  default = "172.31.0.0/16"

}
variable "vpc_name" {
  type = map(any)
  default = {
    "Name" = "tf-example"
  }
}

variable "subnet_cidr" {

  type    = string
  default = "172.31.16.0/20"
}

variable "subnet_name" {
  type = map(any)
  default = {
    "Name" = "my_subnet"
  }
}

variable "sg_name" {
  type = map(any)
  default = {
    "Name" = "allow_tls"
  }
}

variable "ports" {
  type    = list(number)
  default = [22, 3306, 443, 80]
}



variable "instance_Name" {

  type = map(any)
  default = {
    "Name" = "Prod-Server"
  }
}

variable "instance_ami" {
  type    = string
  default = "ami-0c4f7023847b90238"
}

variable "instance_type" {
  type    = string
  default = "t3.micro"
}
